import java.util.Scanner;
import java.util.ArrayList;

public class MittelwertMitMethoden {
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Deklaration von Variablen
		double zahl1;
		double zahl2;
		double m;
		
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		Scanner myScanner = new Scanner(System.in);
		zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ") ;
		zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ") ;
		
//		ArrayList<String> cars = new ArrayList<String>();
//		
//		cars.add("volvo") ;
//		cars.add("volvo2") ;
//		cars.add("volvo3") ;
//		
//		System.out.println(cars);
//		
//		System.out.println("position 1");
//		String car = myScanner.next() ;
//		cars.set(0, car) ;
//		
//		System.out.println(cars);
		
		m = berechneMittelwert(zahl1, zahl2) ;
		
		ausgabe(m) ;
		
		myScanner.close();
	}

	public static double eingabe(Scanner ms, String text)
	{
		System.out.println(text);
		double zahl = ms.nextDouble() ;
		return zahl  ;
	}
	
	public static double berechneMittelwert(double z1, double z2)
	{
		
		double mittelwert = (z1 + z2)/ 2.0;
		
		return mittelwert ;
	}
	
	public static void ausgabe(double m)

	{
		System.out.println("Mittelwert: " + m);
	}
	
}
