import java.util.Scanner;

public class whileSchleife {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Scanner eingabe = new Scanner (System.in) ;
		
		System.out.println("Laufzeit : ");
		int laufzeit = eingabe.nextInt() ;
		
		System.out.println("Kapital zum anlegen : ");
		double kapital = eingabe.nextDouble()	;
		
		System.out.println("Zinsen");
		double zinsen = eingabe.nextDouble() ;
		
		double ergebnis = berechneZinsen(laufzeit, kapital, zinsen) ;
		
		System.out.println(ergebnis);
	}
	
	public static double berechneZinsen(int laufzeit, double kapital, double zinsen)
	{
		int i= 0;
		while (i < laufzeit )
		{
			double ergebnis ;
			 ergebnis = kapital * (zinsen/100);
			 kapital += ergebnis ;
			i++ ;
		}
		return Math.round(kapital*100.0)/100.0 ;
	}

}
