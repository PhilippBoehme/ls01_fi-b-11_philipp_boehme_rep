import java.util.Scanner;

public class doWhileSchleife {

	public static void main(String[] args)
	{
		Scanner eingabe = new Scanner(System.in) ;
		
		System.out.println("bitte einlage angeben : ");
		double einlage = eingabe.nextDouble() ;
		
		System.out.println("Zinssatz : ");
		double zinssatz = eingabe.nextDouble() ;
		
		int i = berechne(einlage, zinssatz) ;
		System.out.println("anzahl Jahre bis zur Million : " + i);
		System.out.println("weitere Eingabe = j, oder beenden = n?");
		char neustart = eingabe.next().charAt(0) ;
		if (neustart == 'j')
		{
			main(args) ;
			
		}else
			System.out.println("Programm-Ende");
		
		
	}
	
	public static int berechne(double einlage, double z)
	{
		int million = 1000000 ;
		int i = 0;
		do 
		{
			double ergebnis ;
			 ergebnis = einlage * (z/100);
			 einlage += ergebnis ;
			 i ++ ;
			
		}while(einlage < million) ;
			
		return i ;
	
	}
}
