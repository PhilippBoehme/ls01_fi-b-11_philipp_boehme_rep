import java.util.Scanner;
import java.util.ArrayList;

public class test {

	public static void main(String[] args) {
		
		
		
		
		while (true) {
				
			int ihreWahl;
			System.out.println("");
			
			ArrayList<Integer> fahrkarten = new ArrayList<>();		
            

            do {
                ihreWahl = fahrkartenbestellungErfassung();

                if (ihreWahl > 3 || ihreWahl < 0 || ihreWahl !=9) {

                    System.out.println("Ihre Wahl " + ihreWahl);
                    System.out.println(">>falsche Eingabe<<");
                }
                else if(ihreWahl != 9)
                {
                    fahrkarten.add(ihreWahl);
                }

            } while (ihreWahl != 9);

			double zuZahlenderBetrag = 0.0;
            
            for (int i = 0; i < fahrkarten.size(); i++) {
                zuZahlenderBetrag = zuZahlenderBetrag + fahrkartenbestellungErfassen(fahrkarten.get(i));
            }
            
			double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(r�ckgabebetrag);

			System.out.println("\nVergessen Sie nicht, die Fahrscheine\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");

		}
	}	
	
	public static int fahrkartenbestellungErfassung() {
		Scanner tastatur = new Scanner(System.in);
		int ihreWahl;

		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
		System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		System.out.println("Bezahlen(9)");

		System.out.printf("Geben Sie das gew�nschte Ticket ein: ");

		ihreWahl = tastatur.nextInt();

		return ihreWahl;
	}

	public static double fahrkartenbestellungErfassen(int fahrkartenart) {
		Scanner tastatur = new Scanner(System.in);

		double anzahlTickets;

		double ticketEinzelpreis = 0;

		if (fahrkartenart == 1) {
			System.out.println("Preis pro Tickes: 2,90� ");
			ticketEinzelpreis = 2.90;
		} else if (fahrkartenart == 2) {
			System.out.println("Preis pro Ticket: 8,60� ");
			ticketEinzelpreis = 8.60;
		} else if (fahrkartenart == 3) {
			System.out.println("Preis pro Ticket: 23,50� ");
			ticketEinzelpreis = 23.50;
		}

		do {
			System.out.print("Wie viele Tickets werden gekauft?(Max. 10): ");
			anzahlTickets = tastatur.nextInt();

			if (anzahlTickets > 10 || anzahlTickets < 0) {

				System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}

		} while (anzahlTickets > 10 || anzahlTickets < 0);

		return ticketEinzelpreis * anzahlTickets;

	}
	

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
			System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					"  ");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben.");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
			System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

	}
}