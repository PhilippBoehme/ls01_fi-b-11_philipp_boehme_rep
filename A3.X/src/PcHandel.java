import java.util.Scanner;

public class PcHandel {

	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		
		String artikel = liesString(myScanner, "was moechten Sie bestellen?" ) ;
		int anzahl = liesInt(myScanner, "Geben Sie die Anzahl ein:") ;
		double preis = liesDouble(myScanner, "Geben sie den Nettopreis ein : ") ;
		double mwst = liesDouble(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:") ;

		// Verarbeiten

		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst) ;
		
		
		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis,mwst);
		

		double  r1 = myScanner.nextDouble() ;
		double  r2= myScanner.nextDouble() ;

		double rWiderstand = reihenschaltung(r1, r2);
		double pWiderstand = parallelschaltung(rWiderstand,r1, r2) ;

	}
	
	public static String liesString(Scanner ms, String text)
	{
		System.out.println(text);
		 String artikel = ms.next() ;
		return artikel;
	}
	
	public static int liesInt(Scanner ms, String text)
	{
		System.out.println(text);
		int anzahl = ms.nextInt() ;
		return anzahl ;
	}
	
	public static double liesDouble(Scanner ms, String text)
	{
		System.out.println(text);
		int wert = ms.nextInt() ;
		return wert ;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst)
	{
		double brutto = nettogesamtpreis * (1 + mwst / 100);
		return brutto ;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
	
	
	
	public static double reihenschaltung(double r1,double r2)
	{
		double widerstand = r1 + r2 ;
		return widerstand ;
	}
	
	public static double parallelschaltung(double rWiderstand,double r1, double r2)
	{
		double widerstand = (r1 * r2) / rWiderstand ;
		return widerstand ;
	}
	
	


}
