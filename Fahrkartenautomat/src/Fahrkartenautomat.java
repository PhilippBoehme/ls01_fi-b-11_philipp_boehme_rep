﻿import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat {
	private static Scanner tastatur;

	public static void main(String[] args) {
		double zuZahlenderBetrag = 0.0;
		double rückgabebetrag;
		int fahrkartenart = 0;
		double zSumme = 0.0;
		char next;
		DecimalFormat f = new DecimalFormat("#0.00#");

		// Fahrkarten bestellen

		while (fahrkartenart != 11) {

			fahrkartenart = fahrkartenArt();
			zuZahlenderBetrag = zuZahlenderBetrag + fahrkartenbestellungErfassen(fahrkartenart);
			zSumme = zuZahlenderBetrag;
			if (fahrkartenart != 11)
				System.out.println("Zwischensumme : " + f.format(zSumme) + "€");

			System.out.println("");
			System.out.println("");
			System.out.println("");
		}

		rückgabebetrag = fahrkarteBezahlen(zSumme);

		// Fahrkarte bezahlen

		rueckgeldAusgabe(rückgabebetrag);
		fahrkarteAusgeben();

		// Endlosmodus
		System.out.println("Möchten sie eine weitere Bestellung tätigen? y/n");
		next = tastatur.next().charAt(0);
		if (next == 'y') {
			main(null);
		} else if (next == 'n') {
			System.err.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n");
		} else {
			System.err.println("falsche Eingabe, Programm startet neu ");
			main(null);
		}

		/*
		 * 5. btye = weil 127 tickets mehr als ausreichend sind 6. zuZahlenderBetrag =
		 * zuZahlenderBetrag * tickets weist zuZahlenderBetrag das ergebnis der
		 * Multiplikation zu
		 */
		tastatur.close();
	}

	public static int fahrkartenArt() {
		Scanner tastatur = new Scanner(System.in);

		String[] fahrkartenart2 = { "Einzelfahrschein Regeltarif AB (1)", "Einzelfahrschein Regeltarif BC (2)",
				"Einzelfahrschein Regeltarif ABC (3)", "Kurzstrecke (4)", "Tageskarte Regeltarif AB (5)",
				"Tageskarte Regeltarif BC (6)", "Tageskarte Regeltarif ABC (7)",
				"Kleingruppen-Tageskarte Berlin AB (8)", "Kleingruppen-Tageskarte Berlin BC (9)",
				"Kleingruppen-Tageskarte Berlin ABC (10)", "Bezahlen(11)\n" };

		for (String i : fahrkartenart2) {
			System.out.println(i);
		}

		System.out.printf("Geben Sie das gewünschte Ticket ein: ");
		int fahrkartenart = tastatur.nextInt();

		return fahrkartenart;
	}

	public static double fahrkartenbestellungErfassen(int fahrkartenart) {
		// Einlesen des Ticketpreises
		DecimalFormat f = new DecimalFormat("#0.00#");
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;

		double[] preise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.40, 24.90 };

		if (fahrkartenart > 0 && fahrkartenart < 11) {
			System.out.println("Preis pro Ticket: " + f.format(preise[fahrkartenart - 1]) + " € ");
			zuZahlenderBetrag = preise[fahrkartenart - 1];
		} else if (fahrkartenart == 11) {
			return zuZahlenderBetrag;
		} else {
			System.err.println(">>falsche Eingabe<<");
			main(null);
		}

		// Einlesen der Ticketanzahl

		System.out.println("Anahl der Tickets : ");
		byte tickets = tastatur.nextByte();

		// Ticketbegrenzung überprüfen

		if (tickets >= 1 && tickets <= 10) {
			zuZahlenderBetrag *= tickets;
		} else {
			System.err.println("Ungültige Anzahl an Tickets ! \n");
			zuZahlenderBetrag = fahrkartenbestellungErfassen(fahrkartenart);
		}

		return zuZahlenderBetrag;

	}

	public static double fahrkarteBezahlen(double zuZahlenderBetrag) {

		DecimalFormat f = new DecimalFormat("#0.00#");
		tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0f;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.println("Noch zu zahlen: " + (f.format(restbetrag)) + " Euro");
			System.out.println("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

			double eingeworfeneMünze = tastatur.nextDouble();

			// Münzeinwurf überprüfen
			if (eingeworfeneMünze > 0 && eingeworfeneMünze <= 2)

				eingezahlterGesamtbetrag += eingeworfeneMünze;
			else {
				System.err.println("falscher Betrag");
			}

		}

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		return rückgabebetrag;

	}

	// drucken der Fahrkarte + Ausgabe
	public static void fahrkarteAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

	}

	// wartet x millisekunden
	public static void warte(int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Geldrückgabe
	public static void rueckgeldAusgabe(double rückgabebetrag) {

		DecimalFormat f = new DecimalFormat("#0.00#");

		if (rückgabebetrag >= 0.0f) {
			System.out.printf("\nDer Rückgabebetrag in Höhe von " + f.format(rückgabebetrag) + " EURO\n");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}

	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("" + betrag + " " + einheit);
	}

}