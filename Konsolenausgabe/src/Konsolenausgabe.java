
public class Konsolenausgabe {

	public static void main(String[] args) {
		System.out.println("Hallo\nwie geht es dir? \b gut\tund selbst\f test");

		String lulu = "Java-Programm" ;
		
		//Standardausgabe
		System.out.printf( "\n|%s|\n", lulu );
		//RechtsbŁndig
		System.out.printf( "|%20s|\n", lulu );
		//LinksbŁndig
		System.out.printf( "|%-20s|\n", lulu );
		//LinksbŁndig 5 zeichen
		System.out.printf( "|%5s|\n", lulu );
		//nur die ersten 4 Zeichen
		System.out.printf( "|%.4s|\n", lulu );
		//RechtsbŁndig + erste 4 stellen 
		System.out.printf( "|%20.4s|\n", lulu ); 
		
		
		int i = 123 ;
		
		System.out.printf( "\n|%d| |%d|\n" , i, -i);
		System.out.printf( "|%5d| |%5d|\n" , i, -i);
		System.out.printf( "|%-5d| |%-5d|\n" , i, -i);
		System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i);
		System.out.printf( "|%05d| |%05d|\n\n", i, -i);
		System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc );
		System.out.printf( "|%08x| |%#x|\n\n", 0xabc, 0xabc );
		
		double d = 1234.5678 ;
		
		System.out.printf( "|%f| |%f|\n" , d, -d);
		System.out.printf( "|%.2f| |%.2f|\n" , d, -d);
		System.out.printf( "|%10f| |%10f|\n" , d, -d);
		System.out.printf( "|%10.2f| |%10.2f|\n" , d, -d);
		System.out.printf( "|%010.2f| |%010.2f|\n", d, -d);
		
		
	}

}
