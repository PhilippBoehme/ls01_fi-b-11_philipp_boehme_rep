
public class Aufgabenblatt_2 {

	public static void main(String[] args) {

		
	// Aufgabe 1
		String s = "*" ;
		
		System.out.printf( "\t%s%s\n", s, s );
		System.out.printf( "%s\t\t%s\n", s, s );
		System.out.printf( "%s\t\t%s\n", s, s );
		System.out.printf( "\t%s%s\n", s, s );
		
	// Aufgabe 2
		
		String a  = "0!";
		String b  = "1!";
		String c  = "2!";
		String d  = "3!";
		String e  = "4!";
		String f  = "5!";
		
		String g = " 1 " ;
		String h = " 2 " ;
		String i = " 3 " ;
		String j = " 4 " ;
		String k = " 5 " ;
		String l = " 6 " ;
		String m = " 24 " ;
		String n = " 120 " ;
		
		
		String ab = "";
		String bb = g;
		String cb = g + "*" + h;
		String db = g + "*" + h + "*" + i; 
		String eb = g + "*" + h + "*" + i + "*" + j ;
		String fb = g + "*" + h + "*" + i + "*" + j + "*" + k ;


		
				
		System.out.printf("\n%-5s=%-19s=%4s", a,ab,g);
		System.out.printf("\n%-5s=%-19s=%4s", b,bb,g);
		System.out.printf("\n%-5s=%-19s=%4s", c,cb,h);
		System.out.printf("\n%-5s=%-19s=%4s", d,db,l);
		System.out.printf("\n%-5s=%-19s=%4s", e,eb,m);
		System.out.printf("\n%-5s=%-19s=%4s", f,fb,n);
		
	// Aufgabe 3
		
		System.out.printf("\n\n%-12s|%10s", "Fahrenheit","Celsius");
		System.out.print("\n-----------------------");
		
		int ad   = -20;
		double bd = -28.8889;
		System.out.printf("\n%+-12d|%10.2f", ad,bd);
		ad  = -10;
		bd = -28.8889;
		System.out.printf("\n%+-12d|%10.2f", ad,bd);
		ad   = 0;
		bd= -23.3333;
		System.out.printf("\n%+-12d|%10.2f", ad,bd);
		
		ad = 10 ;
		bd = -17.7778;
		System.out.printf("\n%+-12d|%10.2f", ad,bd);
		ad   = 20;
		bd = -6.6667;
		System.out.printf("\n%+-12d|%10.2f", ad,bd);
		ad  = 30;
		bd = -1.1111;
		System.out.printf("\n%+-12d|%10.2f", ad,bd);


		

	}

}
